editorconfig-core (0.12.5-2.1) unstable; urgency=medium

  * Non-maintainer upload

  [ Jeremy Bicha ]
  * Add second "orig" tarballs for build tests
  * Update debian/watch & debian/gbp.conf for this to work
  * Update debian/copyright for new component
  * Cherry-pick patches from test repo to match submodule commit
  * debian/libeditorconfig0.symbols: Set Build-Depends-Package
  * debian/rules: Set all hardening flags
  * debian/control: Drop duplicate section field

  [ Debian Janitor ]
  * Set upstream metadata fields

  [ Vagrant Cascadian ]
  * debian/rules: Pass -DCMAKE_BUILD_RPATH_USE_ORIGIN=ON for reproducibility
    (Closes: #1001873)

 -- Jeremy Bicha <jbicha@ubuntu.com>  Fri, 09 Sep 2022 15:42:21 -0400

editorconfig-core (0.12.5-2) unstable; urgency=medium

  * update symbols: 1 public symbols added

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 20 Nov 2021 01:37:13 +0100

editorconfig-core (0.12.5-1) experimental; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * build-depend on libpcre2-dev pkg-config (not libpcre3-dev);
    closes: bug#1000081, thanks to Matthew Vernon
  * use pkgkde-symbolshelper; build-depend on pkg-kde-tools
  * build separately from source
  * update copyright info:
    + update coverage
    + use SPDX shortname BSD-1-Clause
    + sort License sections alphabetically
  * use semantic newlines in long description and copyright fields
  * update patches
  * reference DEP-3 format in patch headers
  * update watch file:
    + simplify regular expression
    + generalize filename mangling
    + set (currently unused) dversionmangle=auto
    + use substitution strings
    + mention gpb --uscan in usage comment
  * install cmake and pkg-config files
  * update symlink to versioned manpage

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 19 Nov 2021 16:17:23 +0100

editorconfig-core (0.12.1-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * use secure https protocol in copyright file Format field URI
  * use package priority optional (not extra) as per policy 4.0.1
  * update Vcs-* fields to use salsa.debian.org

  [ Jonas Smedegaard ]
  * tidy rules file;
    stop build-depend on cdbs devscripts
  * use debhelper compatibility level 13 (not 9);
    build-depend on debhelper-compat (not debhelper)
  * declare compliance with Debian Policy 4.6.0
  * relax to build-depend unversioned on d-shlibs:
    required version satisfied in all supported Debian releases
  * set Rules-Requires-Root: no
  * update copyright info:
    + use Reference field (not License-Reference);
      tighten lintian overrides
    + update coverage
  * drop binary package libeditorconfig0-dbg,
    in favor of auto-generated dbgsym package

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 19 Nov 2021 14:12:22 +0100

editorconfig-core (0.12.1-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/control: Drop explicit Pre-Depends on multiarch-support
    (Closes: #870509).

 -- Aurelien Jarno <aurel32@debian.org>  Sat, 20 Jan 2018 22:17:16 +0100

editorconfig-core (0.12.1-1) unstable; urgency=medium

  [ upstream ]
  * New release.
    + Fix avoid calling exit()
      Closes: Bug#818625.
    + Bump required minimum cmake version to 2.8.7.
    + Use GNU installation dirs for OS portability.

  [ Jonas Smedegaard ]
  * Modernize watch file, and add usage hint comment.
  * Drop CDBS get-orig-source target: Use "gbp import-orig --uscan"
    instead.
  * Declare compliance with Debian Policy 3.9.7.
  * Modernize Vcs-Git field: Use https protocol.
  * Update copyright info:
    + Extend coverage for main upstream author.
    + Extend copyright of packaging to cover current year.
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
  * Add lintian override regarding license in License-Reference field.
    See bug#786450.
  * Build-depend unversioned on debhelper: Needed version satisfied even
    in oldstable.
  * Add lintian override regarding debhelper 9.
  * Update/unfuzz patches.
  * Install development man pages, and adjust section for file format
    man page.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 25 Mar 2016 14:03:18 +0100

editorconfig-core (0.12.0-2) unstable; urgency=medium

  * Fix have -doc package symlink externally packaged jquery.
    Build-depend (not only depend) on libjs-jquery.
  * Update copyright info: Extend coverage for myself.
  * Resolve major version from upstream code (not changelog version.
  * Modernize git-buildpackage config: Drop "git-" prefix.
  * Avoid compressing documentation.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 02 May 2015 14:28:45 +0200

editorconfig-core (0.12.0-1) experimental; urgency=medium

  [ upstream ]
  * New release.
    Closes: bug#767189, Thanks to Hong Xu.

  [ Vasudev Kamath ]
  * Update source URL in copyright and watch file to reflect upstream
    renaming of project to editorconfig-core-c.
  * Update copyright file:
    + Add Files sections for newly added files.
    + Drop Files section for no longer shipped ec_fnmatch.* files, and
      drop now unused BSD-3-clause License section.
  * Build-depend on libpcre3-dev.
  * Declare compliance with Standards-Version 3.9.6.
  * Add patch 1002 to make package build reproducible.

  [ Jonas Smedegaard ]
  * Modernize CDBS use:
    + Stop explicitly pass LDFLAGS: Done implicitly since cdbs 0.4.122.
    + Stop suppress optional build dependencies: available even in
      oldstable.
  * Update Vcs-Browser field to use canonical https protocol.
  * Stop track upstream tarball checksum.
  * Simplify watch file.
  * Fix tighten build-dependency on d-shlibs: Needed for --multiarch and
    --exclude-la options.
  * Unfuzz and reorder patches.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 12 Mar 2015 22:00:51 +0100

editorconfig-core (0.11.5-2) unstable; urgency=medium

  * Bump Standards-Version to 3.9.5, no change to package source.
  * Replace gitweb url with cgit URL in Vcs-Browser field.
  * Mark -dbg package as Multi-Arch same
  * Bump compat to 9 to fix Multi-Arch -dbg package generation

 -- Vasudev Kamath <kamathvasudev@gmail.com>  Thu, 14 Aug 2014 22:18:07 +0530

editorconfig-core (0.11.5-1) unstable; urgency=low

  [ Jonas Smedegaard ]
  * Fix a typo in short description.
  * Add README.source emphasizing control.in file as *not* a
    show-stopper for contributions, referring to
    CDBS+git-buildpackage wiki page for details.

  [ Vasudev Kamath ]
  * Imported Upstream version 0.11.5
  * Moved doxygen into build-depends from build-depends-indep as it is
    generating man pages needed by the package.
    Closes: bug#705682
  * Use DEB_UPSTREAM_VERSION instead of hard coding upstream version in
    arguments to dh_link for editorconfig package.
  * Add multi-arch related quirks, including missing ${cdbs:Pre-Depends}
    substitution value.

 -- Vasudev Kamath <kamathvasudev@gmail.com>  Mon, 10 Jun 2013 19:52:34 +0530

editorconfig-core (0.11.0-1) unstable; urgency=low

  [ Vasudev Kamath ]
  * Initial release.
    Closes: bug#679663.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 29 Mar 2013 16:41:00 +0100
